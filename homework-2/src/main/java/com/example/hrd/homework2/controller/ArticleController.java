package com.example.hrd.homework2.controller;
import com.example.hrd.homework2.model.Article;
import com.example.hrd.homework2.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Controller
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @GetMapping()
    public String index(Model model) {
        List<Article> articles = articleService.getAllArticles();
        model.addAttribute("articles", articles);
        return "index";
    }

    @GetMapping("/create")
    public String createArticle(Model model) {
        model.addAttribute("article", new Article());
        return "create";
    }

    @PostMapping("/create")
    public String createArticle(@ModelAttribute Article article,
                                @RequestParam("file") MultipartFile file) {
        article.setImageURL(filePath(file));
        articleService.addArticle(article);
        return "redirect:/";
    }

    @GetMapping("/{id}/view")
    public String viewArticle(
            Model model,
            @PathVariable() int id
    ) {
        Article article = articleService.findArticleById(id);
        model.addAttribute("article", article);
        return "view";
    }

    @GetMapping("/{id}/delete")
    public String deleteArticle(@PathVariable int id) {
        Article article = articleService.deleteArticleById(id);
        return "redirect:/";
    }

    @GetMapping("/{id}/edit")
    public String editArticle(Model model,
                              @PathVariable int id) {
        Article article = articleService.findArticleById(id);
        model.addAttribute("article", article);
        return "edit";
    }

    @PostMapping("/edit")
    public String editArticle(@ModelAttribute Article article, @RequestParam("file") MultipartFile file) {
        article.setImageURL(filePath(file));
        articleService.editArticle(article);
        return "redirect:/";
    }

    private String filePath(MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            File directory = new File("src/main/resources/files");
            if (!directory.exists()) {
                directory.mkdir();
            }
            Path path = Paths.get("src/main/resources/files/" + fileName);
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            return "/files/" + path.getFileName().toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
